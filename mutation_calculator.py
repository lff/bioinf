# -*- coding: utf-8 -*-
"""
Created on 02/08/2019

.. sectionauthor:: Luis F. de Figueiredo <lfdefigueiredo@gmail.com>
"""
from __future__ import unicode_literals
from os.path import join, basename, dirname
from random import choices

from itertools import product
import hashlib
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO, bgzf
import logging

logging.basicConfig()
logger = logging.getLogger(basename(dirname(__file__)))
logger.setLevel(logging.DEBUG)


def calculate_all_mutations(sequence=''.join(choices('ATGC', k=1000)), insertion_length=9):

    seq_len = len(sequence)

    logger.info(f'expected len is {seq_len}\nseq len:{len(sequence)}')

    all_insertions = [''.join(i) for i in set(product('ATGC', repeat=insertion_length))]
    logger.info(f'expected number of mutations {4**insertion_length}\n'
                f'number of mutations calculated: {len(all_insertions)}')

    with open(join(dirname(__file__), "original_seq.fasta"), "w") as fh:
        SeqIO.write(SeqRecord(Seq(sequence, generic_dna), 'original_sequence', '', ''), fh, "fasta")

    hashset = set()
    count = 0
    with bgzf.BgzfWriter("mutants.fasta.bgz", "ab") as zfh:
            for i in range(len(sequence)+1):
                for j in all_insertions:
                    mut = ''.join([sequence[:i], j, sequence[i:]])
                    count += 1
                    # create a checksum of the sequence and use it to check if we have already generated a similar one
                    checksum_gen = hashlib.md5()
                    checksum_gen.update(mut.encode('utf-8'))
                    # only write to file unique sequences
                    if checksum_gen.hexdigest() not in hashset:
                        SeqIO.write([SeqRecord(Seq(mut, generic_dna), 'p_%i_m_%s' % (i, j), '', '')], zfh, "fasta")
                        hashset.add(checksum_gen.hexdigest())
                    if not count % len(all_insertions):
                        logger.info(f'position: {i}\n'
                                    f'number of calculated mutations: {count}\n'
                                    f'number of recorded mutations: {len(hashset)}')

    logger.info(f'expected number of mutated sequences {1000 * 4**insertion_length}\n'
                f'number of mutated sequences calculated: {count}\n'
                f'number of mutated sequences recorded: {len(hashset)}')

    return


if __name__ == '__main__':
    calculate_all_mutations()
    exit()
